<?php

/**
 * Контроллер для импорта, и нализа Excel документа
 *
 * @version 1.0
 * @author Эльдарханов А.Р.
 */

Class Input_import extends MY_Controller {

    // Для проверки индекса из адреса
    const NAME_POSTCODE = 'индекс';
    // Для проврки дома из адреса
    const NAME_HOME = 'д';

    public function __construct() {
        parent::__construct();
        $this->load->model('factor/m_factor_data_property', 'm_factor_data_property');
        $this->load->model('rubricator/m_rubricator', 'm_rubricator');
        $this->load->model('factor/m_factor_data', 'm_factor_data');
        $this->load->model('map/m_map_objects', 'm_map_objects');
        $this->load->model('subject/m_subject', 'm_subject');
        $this->load->model('factor/m_factor', 'm_factor');
        $this->load->model('files/m_files', 'm_files');
        $this->load->model('fias/m_fias', 'm_fias');
    }

    /**
     * Метод для импорта данных по таблицам.
     * В случае успешного импорта, возвр-ет json объект, со значением true
     * @return array|int|void
     */
    public function import() {
        if(!$this->input->is_ajax_request())
            show_404();

        // Проверка доступности импорта в текущей версии
        if (USE_INPUT_IMPORT == 1) {
            $imp_data = $this->input->get();
            // Получение информации о файле
            $file_inf = $this->get_file($imp_data['file_id']);
            $file_name = FCPATH . $file_inf[0]['file_name'];
            $set_subject = $imp_data['set_subj_default'];

            if($file_name == '')
                exit("Ошибка при чтении Excel файла");

            // Проверка файла на существование в /public/uploads/input_data
            if(!file_exists($file_name))
                exit("Файл не существует");

            $this->load->library('PHPExcel');
            $this->load->library('Excelmanager');

            // создаем excel объект и загружаем файл
            $objReader = PHPExcel_IOFactory::createReaderForFile($file_name);
            $objReader->setReadDataOnly(false);
            $phpExcel = $objReader->load($file_name);

            // получаем активную страницу (проекты перспективные)
            $sheet = $phpExcel->getSheet(0);
            //делаем таблицу соответствия
            $data_index = $this->sheet_analyze_index($sheet, 10);
            //получаем данные, начиная со строки 11
            $data = $this->sheet_analyze_is_id($sheet, 11, $data_index, $set_subject);

            // Если нет ошибок или субъекты,
            // должны быть установлены по умолчанию
            if($data != 1) {
                return $this->set_data($data, $imp_data);
            }
            return $data;
        }

        return print_json_data(array('is_error' => true, 'msg' => '', 'data', 'true'));
    }

    /**
     * Анализ Excel документа с factor_data_id
     *
     * @param $sheet - Excel документ
     * @param $rowIndex - начиная со строки
     * @param $set_subject - если задан субект по умолчанию
     * @param $indexes
     * @return array
     *
     * Если при анализе документа не было найдено ошибок,
     * и предупреждений, то метод вернет
     * array(
     *   '0' => array(
     *       id => 1,
     *       fid => 'Значение fid из файла',
     *       ...
     *   ),
     *   …
     * )
     * В случае предупреждений и ошибок, вернет
     *  array(
     *   'error' => array(
     *       0 => 1 - строка с ошибкой,
     *       ...
     *   ),
     *   'warning' => array(
     *       0 => 1 - строка с предупреждением,
     *       ...
     *   )
     * )
     */
    private function sheet_analyze_is_id($sheet, $rowIndex, $indexes, $set_subject) {
        // максимальное значение строк
        $maxRow = $sheet->getHighestRow();
        // максимальное значение колонок
        $maxCol = $sheet->getHighestColumn();
        // алфавитные значения колонок
        $alphas = $this->get_column_alphas($maxCol);
        // Количество считываемых колонок, для кратаного сравнения
        $arr_fold = count($indexes);
        $data = array();

        for ($row = $rowIndex; $row <= $maxRow; $row++) {
            $data_row = array();
            // Считывание заданного ряда колонок
            foreach ($alphas as $alpCol) {
                $name = $alpCol.$row;

                if (!isset($indexes[$alpCol])) {
                    continue;
                }

                // Текущий factor_id
                $fid = $indexes[$alpCol];
                // Получение типа factor_id
                $node = $this->m_factor->mptt->get_node_byid($fid);

                // factor_id колнки адреса текущего реестра
                if(FACTOR_ADDRESS == $node['data_type']) {
                    $address_fid = $indexes[$alpCol];
                }

                // Значение поля, колонки
                $cell = $sheet->getCell($name)->getValue();

                // Массивы со значениями ячеек
                $arr[] = $data_row[$indexes[$alpCol]] = $cell;
                // Длина массива
                $arr_length = count($arr);

                // Кратное сравнение массива(записывается при каждой $arr_fold'ой длине массива)
                if ($arr_length % $arr_fold == 0) {
                    $check_fields = array();
                    // Колонка id
                    $check_fields['id'] = $data_row['id'];
                    // Колонка субъект
                    $check_fields['subject'] = $data_row['subject'];
                    // Провекрка полей субъект
                    $errors['error'][$row-11] = $this->error_checked($check_fields, $row);
                    // Проверка предупреждений
                    $errors['warning'][$row-11] = $this->warning_checked($check_fields, $row);

                    // Формирование массива со значениями полей
                    $data_row[$indexes[$alpCol]] = $cell;

                    // Получение координат по адресу
                    if (!empty($data_row[$address_fid])) {
                        $data_row['coordinate'] = $this->get_point_address($data_row[$address_fid]);
                    }

                    $data[] = $data_row;
                }
            }
        }

        // Удаление пустых элементов массива
        $errors['error'] = array_diff($errors['error'], array(0, null));
        $errors['warning'] = array_diff($errors['warning'], array(0, null));

        // Ошибки в документе
        if (count($errors['error']) > 0) {
            return print_json_data($errors);
        }
        // Предупреждение
        if (count($errors['warning']) > 0 && $set_subject < 1) {
            return print_json_data($errors);
        }

        return $data;
    }

    /**
     * Ошибки в строках
     * @param $check_fields - колонка для проверки
     * @param $line_error - строка с ошибкой
     * @return $errors - возвращает строку с ошибкой
     */
    private function error_checked($check_fields, $line_error) {
        // Проверка корректности введенных субъектов
        if (empty($check_fields['id']) && !empty($check_fields['subject'])) {
            $subject = $this->check_column_subject($check_fields['subject']);
            if ($subject == 0) {
                $errors = $line_error;
                return $errors;
            }
        }
    }

    /**
     * Предупреждения в строках
     * @param $check_fields - колонка для проверки
     * @param $line_error - строка с предупреждением
     * @return $errors - возвращает строку с предупреждением
     */
    private function warning_checked($check_fields, $line_error) {
        if (empty($check_fields['id']) && empty($check_fields['subject'])) {
            $errors  = $line_error;
            return $errors;
        }
    }

    /**
     * Метод провекрки субъукта, установленного по умаолчанию
     * @param $subject_id - id субъекта
     * @return mixed 1 - id субъектa существует, 0 - id субъекта не существует
     */
    private function check_subject_default($subject_id) {
        if (empty($subject_id)) {
            return null;
        }

        $subject_id = intval($subject_id);
        $subject = $this->m_subject->check_subject_by_id($subject_id);

        return $subject;
    }

    /**
     * Метод провекрки субъукта, в колонке субъект
     * @param $subject_name - название субъекта
     * @return mixed 1 - субъект существует, 0 - субъект не существует
     */
    private function check_column_subject($subject_name) {
        if (empty($subject_name)) {
            return null;
        }

        $subject = $this->m_subject->check_column_subject($subject_name);
        return $subject;
    }

    /**
     * Получение координаты точки, по заданному адресу
     * @param $address - адрес
     * @return - координаты or null
     */
    private function get_point_address($address){
        $url = 'http://geocode-maps.yandex.ru/1.x/?format=json&geocode='.$address;
        $response = json_decode(file_get_contents($url));
        $coordinates = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;

        return $coordinates ? $coordinates : null;
    }

    /**
     * Анализ страницы excel, для factor_id
     * выборка их в массивы
     * @param $sheet  - страница excel
     * @param $row
     * @return array
     *
     * Врзвращает букву колонки, в качестве индекса массивы
     * и значение fid в виде
     * array(
     *     A => fid,
     *     B => fid,
     *     ...
     * )
     */
    private function sheet_analyze_index($sheet, $row) {
        //максимальное значение колонок
        $maxCol = $sheet->getHighestColumn();
        //алфавитные значения колонок
        $alphas = $this->get_column_alphas($maxCol);
        $indexArray = array();
        foreach ($alphas as $alpCol) {
            // Буква калонки и номер строки анализ-го fid
            $name = $alpCol.$row;
            $cell = $sheet->getCell($name)->getValue();

            if ($cell == 'subject'){
                $indexArray[$alpCol] = 'subject';
                continue;
            }

            $val = $cell;

            if ($val != '') {
                $indexArray[$alpCol] = $val;
            }
        }
        return $indexArray;
    }

    /**
     * Формирование имен колонок excel страницы,
     * начиная с A до ZZ, при этом выбор идет до максимальной колонки
     * @param $maxCol
     * @return array - возвращает массив букв, анализируемых колонок
     * array(
     *     0 => A,
     *     1 => B,
     *     ...
     * )
     */
    private function get_column_alphas($maxCol) {
        $alphasSys = range('A', 'Z');
        $alphas = array();

        //однобуквенные колонки
        foreach ($alphasSys as $alp) {
            $alphas[] = $alp;
            if ($maxCol == $alp) {
                return $alphas;
            }
        }

        //двухбуквенные колонки
        foreach ($alphasSys as $alpOut) {
            foreach ($alphasSys as $alpIn) {
                $name = $alpOut.$alpIn;
                $alphas[] = $name;
                if ($maxCol == $name) {
                    return $alphas;
                }
            }
        }

        return $alphas;
    }

    /**
     * Метод для получения ин-ии о файле
     * @param $file_id - ИД файла
     * @return - Массив с инф. о файле или null
     */
    private function get_file($file_id) {
        if (empty($file_id)) {
            return null;
        }

        $file_id = intval($file_id);
        return $this->m_files->get_file_id($file_id);
    }

    /**
     * Метод для получения названия субъекта
     * по его ид
     * @param $subject_id - ид субъекта
     * @return null|$subject_name - название субъекта(Ростов-на-Дону)
     */
    private function convert_subject($subject_id) {
        if (empty($subject_id)) {
            return null;
        }

        $subject_id = intval($subject_id);
        $subject_name = $this->m_subject->convert_subject($subject_id);

        return $subject_name;
    }

    /**
     * Запись данных в реестр
     * @param $data - массив с данным для записи в БД
     * @param $registry_params - различные параметры для записи данных в БД
     * @return в случае успешной записи, возвр-ет json объект, со значением true
     */
    private function set_data($data, $registry_params) {
        // Проверка данных на существование
        if (empty($registry_params)) {
            exit('Параметры реестра, не были получены');
        }
        if (empty($data)) {
            exit('Данные для импорта, не были получены');
        }

        // Ид реестра
        $registry_id = intval($registry_params['factor_id']);
        // Установить субъект по умолчанию
        $set_subj_default = intval($registry_params['set_subj_default']);
        // Ид субъекта по умолчанию
        $default_subject_id = intval($registry_params['subject_id']);

        // Дата периода(версии)
        if (!empty($registry_params['scheduler_date']) && !is_null($registry_params['scheduler_date'])) {
            $scheduler_date = $registry_params['scheduler_date'];
        } else {
            $scheduler_date = null;
        }

        // Проверка ид субъекта на существование
        $subject = $this->check_subject_default($default_subject_id);

        if ($subject !== 1) {
            exit('Субъект по умолчанию, содержит не корректные данные');
        }

        // Получение субъекта по его ид
        $subject_name = $this->convert_subject($default_subject_id);

        // Установить субъект по умолчанию
        if ($set_subj_default == 1) {
            // Установка субъекта по умолчанию. В полях, где нет ид
            for ($i = 0; $i < count($data); $i++) {
                if (empty($data[$i]['id']) && empty($data[$i]['subject'])) {
                    $data[$i]['subject'] = $subject_name;
                }
            }
        }

        //проход по каждой записи из импортируемой таблицы
        foreach ($data as $values) {
            // Получение ид записи реестра
            $factor_data_id = $this->get_factor_data_id($default_subject_id, $registry_id, $scheduler_date);
            // Получение номера строки таблицы
            $tbl = $this->m_factor_data->get_by_id($factor_data_id);
            $tbl = $tbl['reestr_tbl'];

            // Вставка главного показателя
            $parent_id = $this->m_factor_data_property->insert_data_main($factor_data_id, $registry_id, $tbl);

            // Запись данных в БД
            foreach ($values as $factor_id => $value) {
                // Для определения типа factor_id
                $node = $this->m_factor->mptt->get_node_byid($factor_id);

                // Колонки для записи
                if (is_numeric($factor_id)) {
                    // Параметры для записи
                    $formatted_data = array();
                    $formatted_data['factor_data_id'] = intval($factor_data_id);
                    $formatted_data['factor_id'] = intval($factor_id);
                    $formatted_data['data_parent'] = intval($parent_id);
                    $formatted_data['data_tbl'] = intval($tbl);

                    // Определение типа колонки
                    switch ($node['data_type']) {
                        case FACTOR_DIGIT: // х-ка число
                            if (FACTOR_DIGIT_FLOAT == $node['number_type']) {
                                $ratio = $this->m_factor->get_ratio($factor_id);
                                $formatted_data['data_digit'] = floatval($value) * $ratio;
                            } else {
                                $formatted_data['data_digit'] = intval($value);
                            }
                            break;
                        case FACTOR_STRING:
                        case FACTOR_TEXT: // х-ка строка и текст
                            $formatted_data['data_str'] = $value;
                            break;
                        case FACTOR_LIST: // х-ка список (рубрикатор)
                            // Получение ИД рубрикатора
                            $value = $this->m_rubricator->get_rubricator_id($value)->id;
                            $formatted_data['data_rubricator'] = intval($value);
                            break;
                        case FACTOR_ADDRESS: // х-ки адреса
                            // Адрес из файла
                            $parent_addr = $this->address_fromat($value);
                            // Адрес полученный из Ya-x
                            $yandex_addr = $this->get_address_yandex($value);

                            // Адрес из файла, в формате [formalname][shortname]
                            $divided_address = $this->divided_address($parent_addr);
                            // Отформатированный адрес для записи в БД
                            $formatted_addr = $this->get_guide_address($divided_address, $yandex_addr);

                            // Данные объекта для записи
                            $formatted_data['data_region'] = $formatted_addr['data_region'];
                            $formatted_data['data_mregion'] = $formatted_addr['data_mregion'];
                            $formatted_data['data_city'] = $formatted_addr['data_city'];
                            $formatted_data['data_street'] = $formatted_addr['data_street'];
                            $formatted_data['data_postcode'] = $formatted_addr['data_postcode'];
                            $formatted_data['data_home'] = $formatted_addr['data_home'];
                            break;
                        case FACTOR_DATE: // х-ка дата
                            $formatted_data['data_from_year'] = $value;
                            break;
                        /** TODO: Для дальнейщей перспективы
                        case FACTOR_LINK: // х-ки ссылки
                            $data['data_link'];
                            $data['data_link_name'];
                            break;
                        case FACTOR_FILE: // х-ка файл
                            $data['data_file'];
                            $data['data_file_name'];
                            break;
                        case FACTOR_GALLERY: // х-ка галереи
                            $data['data_gallery'];
                            break;
                        case 'alter': // альтернативный показатель
                            $data['data_alter'];
                            break;
                        case 'store':
                            break;
                        */
                    }

                    // Схранение в БД
                    $this->m_factor_data_property->save($formatted_data);
                }
            }

            // Преоброзование координат в точку (POINT wkt)
            if (!empty($values['coordinate'])) {
                $coordinate = str_replace(' ', ',', $values['coordinate']);
                $geoJSON = '{"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":['.$coordinate.']}}]}';

                // Запись данных координат точки в БД
                $this->m_map_objects->set_data($geoJSON, $factor_data_id);
            }
        }
        //return print_json_data('true');
        return print_json_data(array('is_error' => false, 'msg' => '', 'data' => 'true'));
    }

    /**
     * @param $divided_address - Адрес полученный из ипортируемого файла
     * @param $addr_from_yandex - Адрес полученный из YandexApi
     * @return array|null
     * Массив с данными полученные из адреса импортируемого файла,
     * или адреса полученного из yandexApi
     * array(
     *       'data_region' => 'aoguid региона из адреса',
     *       'data_mregion' => 'aoguid района из адреса',
     *       'data_city' => 'aoguid города из адреса',
     *       'data_street' => 'aoguid улица из адреса',
     *       'data_home' => 'номер дома из адреса',
     *       'data_postcode' => 'индекс из адреса'
     * )
     */
    private function get_guide_address($divided_address, $addr_from_yandex) {
        // guid'ы из адреса, полученного из ипортируемого файла
        // Регион обязателен, для получения guid'a
        if (is_array($divided_address) && !empty($divided_address[0]['formalname'])) {
            // Уровень адресного объекта. Для получения региона
            $divided_address[0]['aolevel'] = M_fias::REGION_LEVEL;

            // Guid и другие параметры региона
            $region_data = $this->m_fias->get_guide_region_file($divided_address[0]);
            // Получение адреса, индекса
            $home_postcode = $this->get_home_postcode($divided_address);

            // Информация по адресу
            for ($i = 0; $i <= count($divided_address); $i++) {
                $data_address['regioncode'] = intval($region_data[0]['regioncode']);
                $data_address['formalname'] = $divided_address[$i]['formalname'];
                $data_address['shortname'] = $divided_address[$i]['shortname'];

                // GUID адреса
                $address_info[] = $this->m_fias->get_guide_address_file($data_address);

                switch ($address_info[$i]->aolevel) {
                    case M_fias::REGION_LEVEL: // Регион
                        $formatted_address['data_region'] = $address_info[$i]->aoguid;
                        break;
                    case M_fias::MREGION_LEVEL: // Район
                        $formatted_address['data_mregion'] = $address_info[$i]->aoguid;
                        break;
                    case M_fias::CITY_LEVEL: // Город
                        $formatted_address['data_city'] = $address_info[$i]->aoguid;
                        break;
                    case M_fias::STREET_LEVEL: // Улица
                        $formatted_address['data_street'] = $address_info[$i]->aoguid;
                        break;
                    default: // Адрес, индекс
                        if (!empty($home_postcode['data_home'])) {
                            $formatted_address['data_home'] = $home_postcode['data_home'];
                        } elseif (!empty($home_postcode['data_postcode'])) {
                            $formatted_address['data_postcode'] = $home_postcode['data_postcode'];
                        }
                        break;
                }
            }

            // Если не удалось получить guid'ы из адреса, полученного из импортируемого файла
            if ((empty($formatted_address['data_region'])) && (empty($formatted_address['data_city'])) &&
                (empty($formatted_address['data_street'])) && (empty($formatted_address['data_mregion'])) &&
                (!empty($addr_from_yandex))) {

                // Регион обязателен, для получения guid'a
                $region = $this->address_fromat($addr_from_yandex['region']);
                $region_info['offname'] = $region[0][0];
                // Уровень адресного объекта. Для получения региона
                $region_info['aolevel'] = M_fias::REGION_LEVEL;

                // Если данные о регионе были получены
                if (!empty($region_info)) {
                    // Guid и другие параметры региона
                    $region_data_yandex = $this->m_fias->get_guide_region_yandex($region_info);

                    if (!empty($region_data_yandex)) {
                        // Город, село
                        $city = $this->address_fromat($addr_from_yandex['city']);
                        // Район
                        $mregion = $this->address_fromat($addr_from_yandex['mregion']);
                        // Улица
                        $street = $this->address_fromat($addr_from_yandex['street']);

                        $data_yandex_addr = array();
                        $data_yandex_addr['mregion'] = $mregion[0][0];
                        $data_yandex_addr['city'] = $city[0][0];
                        $data_yandex_addr['street'] = $street[0][0];

                        // Информация по адресу
                        foreach($data_yandex_addr as $shortname => $offname) {
                            $data_addr['offname'] = $offname;
                            $data_addr['regioncode'] = $region_data_yandex[0]['regioncode'];
                            // GUID адреса
                            $yandex_address_info[$shortname] = $this->m_fias->get_guide_address_yandex($data_addr);
                        }

                        // Информация по адресу
                        $formatted_addr_yandex = array();
                        $formatted_addr_yandex['data_region'] = $region_data_yandex[0]['aoguid'];
                        $formatted_addr_yandex['data_mregion'] = $yandex_address_info[0]['mregion'][0]['aoguid'];
                        $formatted_addr_yandex['data_city'] = $yandex_address_info['city'][0]['aoguidd'];
                        $formatted_addr_yandex['data_street'] = $yandex_address_info['street'][0]['aoguid'];
                        $formatted_addr_yandex['data_home'] = $home_postcode['data_home'];
                        $formatted_addr_yandex['data_postcode'] = $home_postcode['data_postcode'];

                        // Параметры адреса полученного из yandexApi
                        return $formatted_addr_yandex;
                    }
                }
            }
            // Параметры адреса полученного из ипорт-го файла
            return $formatted_address;
        }
    }

    /**
     * Возвращает дом и индекс субъекта
     * @param $data
     * @return null|$formatted_data
     * array(
     *       'data_home' => 'номер дома из адреса',
     *       'data_postcode' => 'индекс из адреса'
     * )
     */
    private function get_home_postcode($data) {
        if (is_array($data)) {
            // Индекс, дом
            for ($i = 0; $i <= count($data); $i++) {
                if (Input_import::NAME_HOME == $data[$i]['shortname']) { // Дом
                    // Если адрес содержит некорректные данные
                    if (!preg_match('/[^\-a-zA-Zа-яА-ЯёЁ\s]/u', $data[$i]['formalname'])) {
                        $formatted_address['data_home'] = null;
                    } else {
                        // Если адрес содержит корректные данные
                        $formatted_data['data_home'] = $data[$i]['formalname'];
                    }
                } elseif (Input_import::NAME_POSTCODE == $data[$i]['shortname']) { // Индекс
                    $formatted_data['data_postcode'] = $data[$i]['formalname'];
                }
            }
            return $formatted_data;
        }
        return null;
    }

    /**
     * Возвращает форматированный адрес
     * полученный из YandexApi
     * @param $address - адрес из файла
     * @return array
     * array(
     *       'region' => 'название региона',
     *       'mregion' => 'название района',
     *       'city' => 'название город или село',
     *       'street' => 'название улицы',
     *       'description' => 'Форматированный yandex'ом адрес запроса',
     *       'request' => ' запрашиваемый адрес из файла',
     *       'Координаты адреса' => ' зкоординаты адреса в виде - 42.229927 47.538752',
     * )
     */
    private function get_address_yandex($address) {
        $url = 'http://geocode-maps.yandex.ru/1.x/?format=json&geocode='.$address;
        $response = json_decode(file_get_contents($url));

        // Отформатированный адрес
        $formatted = array();

        // Регион
        $formatted['region'] =
            $response->response->GeoObjectCollection->featureMember[0]->
            GeoObject->metaDataProperty->GeocoderMetaData->
            AddressDetails->Country->AdministrativeArea->AdministrativeAreaName;
        // Район
        $formatted['mregion'] =
            $response->response->GeoObjectCollection->featureMember[0]->
            GeoObject->metaDataProperty->GeocoderMetaData->
            AddressDetails->Country->AdministrativeArea->
            SubAdministrativeArea->SubAdministrativeAreaName;
        // Город, село
        $formatted['city'] =
            $response->response->GeoObjectCollection->featureMember[0]->
            GeoObject->metaDataProperty->GeocoderMetaData->
            AddressDetails->Country->AdministrativeArea->
            SubAdministrativeArea->Locality->LocalityName;
        // Улица
        $formatted['street'] =
            $response->response->GeoObjectCollection->featureMember[0]->
            GeoObject->metaDataProperty->GeocoderMetaData->
            AddressDetails->Country->AdministrativeArea->
            SubAdministrativeArea->Locality->Thoroughfare->
            ThoroughfareName;
        // Форматированный адрес запроса
        $formatted['description'] =
            $response->response->GeoObjectCollection->featureMember[0]->
            GeoObject->description;
        // Запрашиваемый адрес
        $formatted['request'] =
            $response->response->GeoObjectCollection->metaDataProperty->
            GeocoderResponseMetaData->request;
        // Координаты адреса
        $formatted['coordinate'] =
            $response->response->GeoObjectCollection->featureMember[0]->
                GeoObject->Point->pos;

        return $formatted;
    }

    /**
     * Возвращает массив, содержащий тип и наименование объекта
     * @param $address - адрес
     * @return array
     * array(
     *   '0' => array(
     *       0 => 'наименование области в виде - Ростовская',
     *       fid => 'тип объекта в виде - обл',
     *   ),
     *   …
     * )
     */
    private function address_fromat($address) {
        if (!empty($address)) {
            // Разделение адреса на тип объекта, и наименование
            $addr = explode(',', $address);

            // Формирование наименования и типа объекта
            for ($i = 0; $i < count($addr); $i++) {
                if (trim($addr[$i]) != '') {
                    // Разделение объекта на тип и наименование
                    $divided_adddr[] = array_diff(explode(' ', $addr[$i]), array(0, ''));
                    // Если нименование региона, улицы, состоит из нескольких слов
                    if (count($divided_adddr[$i]) > 2) {
                        $str = $divided_adddr[$i];

                        // Тип объекта
                        $result[] = $str[1];
                        // Наименование
                        $result[] = str_replace($result[0], '', implode(' ',  $str));

                        $divided_adddr[$i] = $result;
                    }
                }
            }
            return $divided_adddr;
        }
        return null;
    }

    /**
     * Возвращает массив в виде:
     * @param $address
     * @return null | $formatted_addr
     * array(
     *   'formalname' => 'наименование'
     *   'shortname' => 'тип объекта'
     * )
     */
    private function divided_address($address) {
        if(is_array($address) && !empty($address)) {
            for($i = 0; $i < count($address); $i++) {
                $formatted_addr[$i] = array(
                    'formalname' => trim(preg_replace('/[^\-a-zA-Zа-яА-ЯёЁ0-9\s]/u', '', min($address[$i]))),
                    'shortname' => trim(preg_replace('/[^\-a-zA-Zа-яА-ЯёЁ0-9\s]/u', '', max($address[$i]))),
                     //'formalname' => min($address[$i]),
                     //'shortname' => max($address[$i]),
                );
            }
            return $formatted_addr;
        }
        return null;
    }

    /**
     * Получение ид данных головного показателя
     * @param $subject_id - ид субъекта
     * @param $factor_id - ид показателя
     * @param $scheduler_date - дата периода(версии)
     * @return int fid в виде - 945612
     */
    private function get_factor_data_id($subject_id, $factor_id, $scheduler_date = null) {
        $uid = $this->user->get_user_id();
        $date = (new DateTime());
        $format = $date->format('Y-m-d H:i:s');

        return $this->m_factor_data->add_new_reestr_row(
            $factor_id, $subject_id,
            $scheduler_date, null, $format, $uid
        );
    }
}